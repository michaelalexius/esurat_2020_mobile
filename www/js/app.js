"use strict";
// Dom7
var $$ = Dom7;

// Init App
var app = new Framework7({
	root: '#app',
	theme: 'md',
	routes: routes,
	view: {
		pushState: true,
	}
});

var mainView = app.views.create('.view-main', {
	url: './index.html'
});


var myPhotoBrowserPage = app.photoBrowser.create({
    photos : [
        'https://cdn.framework7.io/placeholder/sports-1024x1024-1.jpg',
        'https://cdn.framework7.io/placeholder/sports-1024x1024-2.jpg',
        'https://cdn.framework7.io/placeholder/sports-1024x1024-3.jpg',
    ],
    type: 'page',
    pageBackLinkText: 'Back'
});
$$('.pb-page').on('click', function () {
    myPhotoBrowserPage.open();
});





