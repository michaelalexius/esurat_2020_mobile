"use strict";
var url = "http://api-esurat-madiun.prototipe.net";
var routes = [
  // Index page
  {
    path: '/',
    url: './index.html',
  },
  {
    path: '/inbox/',
    url: './inbox.html',
  },
  {
    path: '/outbox/',
    url: './outbox.html',
  },
  {
    path: '/disposisi/',
    url: './disposisi.html',
  },
  {
    path: '/menu/',
    url: './menu.html',
  },
  {
    path: '/compose/',
    url: './compose.html',
  },
  {
    path: '/cari/',
    url: './carisurat.html',
  },
  {
    path: '/composetulis/',
    url: './composetulis.html',
  },
  {
    path: '/calendar/',
    url: './calendar.html',
  },
  {
    path: '/detailsurat/',
    url: './detailsurat.html',
  },
  {
    path: '/detailsuratkeluar/',
    url: './detailsuratkeluar.html',
  },
  {
    path: '/inputdisposisi/',
    url: './inputdisposisi.html',
  },
  {
    path: '/home2/',
    url: './home2.html',
  },

  {
    path: '/home3/',
    url: './home3.html',
  },
  
  {
    path: '/home4/',
    url: './home4.html',
  },
 
  {
    path: '/single-post/',
    componentUrl: './single-post.html',
  },
  {
    path: '/single-post-gallery/',
    componentUrl: './single-post-gallery.html',
  },
  {
    path: '/image-slide-post-gallery/',
    url: './image-slide-post-gallery.html',
  },
  {
    path: '/single-post-video/',
    componentUrl: './single-post-video.html',
  },
  {
    path: '/login/',
    url: './login.html',
  },
  {
    path: '/author/',
    url: './author.html',
  },
  {
    path: '/forgot-password/',
    url: './forgot-password.html',
  },
  {
    path: '/contact/',
    url: './contact.html',
  },
  {
    path: '/register/',
    url: './register.html',
  },
  {
    path: '/categories-detail/',
    url: './categories-detail.html',
  },

  {
    path: '/bookmark/',
    url: './bookmark.html',
  },

  {
    path: '/search-page/',
    url: './search-page.html',
  },

  

  {
    path: '/accordion/',
    url: './accordion.html',
  },

  {
    path: '/cards/',
    url: './cards.html',
  },

  {
    path: '/tags/',
    url: './tags.html',
  },

  {
    path: '/swiper-slider/',
    url: './swiper-slider.html',
  },

  {
    path: '/horizontal-swiper/',
    url: './horizontal-swiper.html',
  },

  {
    path: '/vertical-swiper/',
    url: './vertical-swiper.html',
  },

  {
    path: '/swiper-space-between/',
    url: './swiper-space-between.html',
  },

  {
    path: '/swiper-fade-effect/',
    url: './swiper-fade-effect.html',
  },

  {
    path: '/swiper-overlap/',
    url: './swiper-overlap.html',
  },

  {
    path: '/swiper-infinite-loop/',
    url: './swiper-infinite-loop.html',
  },

  {
    path: '/button/',
    url: './button.html',
  },

  {
    path: '/profile-author/',
    url: './profile-author.html',
  },

  {
    path: '/pagination/',
    url: './pagination.html',
  },

  
];
