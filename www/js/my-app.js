var $$ = Dom7;
const url = "https://dev2-esurat.madiunkota.go.id";
const urlfile = "https://dev2-esurat.madiunkota.go.id/surat/lampiran/";
var API_TTE= "" //todo jgn lupa pake api yg live kalo mau dijadiin apk
var API_TTE_USERNAME =""
var API_TTE_PASSWORD=""
const URL_SURAT_TTE = "https://esurat.madiunkota.go.id/surat/";
const storageLocation = 'file:///storage/emulated/0/';
var arrTembusan = [];
var arrKepada = [];
var app = new Framework7({
	root: '#app',
	name: 'E-Surat Madiun',
	id: 'com.esurat.f7app',
	panel: { swipe: 'left' },
	theme: 'md',view:
	{
	    iosDynamicNavbar: false,
	},
	routes: [
	  {
	    path: '/',
	    url: './index.html',
	  },
	  {
	    path: '/inbox/',
	    url: './inbox.html',
	  },
	  {
	    path: '/outbox/',
	    url: './outbox.html',
	  },
	  {
	    path: '/disposisi/',
		url: './disposisi.html',
		on: {
			pageInit: function (e,page)
			{
				var kolokDipilih = localStorage.kolok;
				var urlRequest = url+'/api/dashboard/div_disposisi/'+localStorage.user_id;
				// var urlRequest = "10.212.24.89/gg/test.php";
				var role = "";
				if(localStorage.username == "admin")
				{
					role = "superadmin";
				}            
				var fd = new FormData();    
				fd.append( 'role', role );
				fd.append( 'kolok', kolokDipilih );

				if(localStorage.kolok == "nofilter"){
					app.dialog.alert('Mohon Pilih Kolok Terlebih Dahulu');
				}else{
					app.preloader.show();
					app.request({
						url: urlRequest,
						method:'post',
						headers:
							{
							'Authorization': 'Bearer ' + localStorage.apiToken
						}
						,
						contentType:'multipart/form-data',
						data:{
							'role':role,
							'kolok':kolokDipilih
						},
						success:function(data){
							var returned = JSON.parse(data);
							var dataDisposisi = returned['data'];
							var appendToView = "<h3>Data Surat Disposisi : </h3>";
							for (var i = 0; i <= dataDisposisi.length-1; i++) 
							{
								if(dataDisposisi != null)
								{
									appendToView = appendToView + '<div class="card"><div class="card-content" style="margin: 10px;"><a href="/detailsurat/'+dataDisposisi[i]['id']+'/disposisi/"><table><tr><td>'+dataDisposisi[i]['nomor_surat']+' <b>('+dataDisposisi[i]['tanggal_masuk_surat']+')</b></td></tr><tr><td><b>Nomor Agenda :</b>'+dataDisposisi[i]['nomor_agenda']+'</td></tr><tr><td><b>Lampiran :</b> '+ dataDisposisi[i]['lampiran'] +' Lembar</td></tr><tr><td><b>Perihal :</b> '+dataDisposisi[i]['perihal']+'</td></tr></table></a></div></div><br>';
								}
								
								// 
							}
							var jumlahData = dataDisposisi.length;
							if (jumlahData < 1)
							{
								appendToView = "<h4>Tidak ada surat disposisi.</h4>";
							}
							$$("#kotakdisposisi").html(appendToView);
							// console.log(dataKotakMasuk);
							app.preloader.hide();
						},
						error: function(err) {
						app.preloader.hide();
						console.log(err);
						}
					})
				}
			},
		}
	  },
	  {
	    path: '/tte_page/',
		url: './tte_page.html',
		on: {
			pageInit: function (e,page)
			{
				if(localStorage.id_dokumen == "" || localStorage.id_dokumen  == "null"){
					$$(".title-info").hide();
				}
				else{
					$$(".title-info").show();
				}
				

				if(localStorage.id_surat != "" && localStorage.id_surat  != "null"){
					var fixlink = URL_SURAT_TTE + "surat_" + localStorage.id_surat  +".pdf";
					var linksurat = '<a class="link external clink" href="'+ fixlink + '">File Surat surat_'+ localStorage.id_surat  +".pdf"+'</a>';
					$$("#filesurat_tte").html(linksurat);
					$$("#ttepage_nik").val(localStorage.no_ktp);
				}

				$$(document).on('click','.downloadtte',function(e){
					e.preventDefault();
					e.stopImmediatePropagation();
					var linkdownload = $$(this).data('link');
					moveFile(linkdownload);
				});
				$$(document).on('click','#tte_kirim',function(e){
					e.preventDefault();
					e.stopImmediatePropagation();
					app.preloader.show();
					//predata
					
					var fixlink = URL_SURAT_TTE + "surat_" + localStorage.id_surat  +".pdf"
					var ktp =  localStorage.no_ktp
					var passPh = $$("#passphrase").val()
					var tamp = "invisible";
					var jenisResponse = "BASE64";
					var fileName = "surat_" + localStorage.id_surat + ".pdf";
					var urlRequest = url+'/api/kotak_keluar/tte_sign/';

					var form_data = new FormData();
					form_data.append("id", localStorage.id_surat);
					form_data.append("file", fileName);
					form_data.append("nik", ktp);
					form_data.append("passphrase", passPh);
					form_data.append("tampilan", tamp);
					form_data.append("jenis_response", jenisResponse);
					form_data.append("_token", btoa(API_TTE_USERNAME + ":" + API_TTE_PASSWORD));
					

					$.ajax({
						url: urlRequest,
						method: "POST",
						headers:
							{
								'Authorization': 'Bearer ' + localStorage.apiToken
							}
						,
						data : form_data,
						async:false,
						contentType: false,
						cache: false,
						processData: false,
						
						beforeSend:function(){
							app.preloader.show();
						},
						success: function (data) {
							app.preloader.hide();
							$$("#passphrase").val("");
							app.dialog.alert("Berhasil! Surat Berhasil di TTE");
							
							
						},
						error: function(err) {
							app.preloader.hide();
							app.dialog.alert(err['responseText']);
						}
					});
					
				});
				
			},
		}
	  },
	  {
	    path: '/menu/',
	    url: './menu.html',
	  },
	  {
	    path: '/compose/',
	    url: './compose.html',
	  },
	  {
	    path: '/cari/',
	    url: './carisurat.html',
	  },
	  {
	    path: '/composetulis/',
	    url: './composetulis.html',
	  },
	  {
	    path: '/ds_riwayat/',
		url: './detailsurat_riwayat.html',
		on: {
	    	pageInit: function (e, page) 
	        {
				//riwayat
				$$(".text_idsurat").html(localStorage.idsurat);
				var urlRequestKomentar = url+'/api/history_surat/';
				app.preloader.show();
				app.request({
					url: urlRequestKomentar,
					method:'post',
					headers:
						{
						  'Authorization': 'Bearer ' + localStorage.apiToken
						}
					,
					contentType:'multipart/form-data',
					data:{
						surat_id :localStorage.idsurat
					},
					success:function(data)
					{
						try
						{
							app.preloader.hide();
							var returned = JSON.parse(data);
							var data_riwayat = '';
							var year = "";
							for (var i = 0; i < returned['data'].length ; i++) 
							{
								var timesplit = returned['data'][i]['time'].split(" ");
								var datesplit = timesplit[0].split("-");
								var jamsplit = timesplit[1].split(".");
								//tahun
								if(year != datesplit[0]){
									year = datesplit[0];
									data_riwayat = data_riwayat + "<h2>"+year+"</h2><br>";
								}
								var dateKiri = getShortDateMonthName(timesplit[0]);
								data_riwayat = data_riwayat +
								'<div class="timeline-item">\
								<div class="timeline-item-date bold">'+ dateKiri +' </div>\
								<div class="timeline-item-divider bggreen"></div>\
								<div class="timeline-item-content">\
								<div class="timeline-item-time">'+jamsplit[0] +'</div>\
								<div class="timeline-item-title cdark">'+ returned['data'][i]['action']  +'</div>\
								<div class="timeline-item-subtitle">'+ returned['data'][i]['name'] + 
								' [' + returned['data'][i]['username'] + '] - ' + returned['data'][i]['jabatan'] +'</div>\
								</div>\
								</div>';
							}

							if(returned['data'].length < 1){
								data_riwayat = "<h4>Tidak ada riwayat surat.</h4>";
							}
							$$("#ds_riwayatisi").html(data_riwayat);
						}
						catch(err)
						{
							app.preloader.hide();
							app.dialog.alert("Komentar tidak dapat ditampilkan.");
						}
						
					},
					error: function(err) {
					  app.dialog.alert("status error : "+err['status']+" error msg : "+err['statusText']+" url : " + urlRequestKomentar);
					}
				})
			},
		}
	  },
	  {
	    path: '/ds_metadata/',
		url: './detailsurat_metadata.html',
		on: {
	    	pageInit: function (e, page) 
	        {
				var urlRequest = url+'/api/kotak_masuk/detail_surat/'+localStorage.idsurat;
				$$(".dsm_id").html(localStorage.idsurat);
				app.preloader.show();
				app.request({
					url: urlRequest,
					method:'post',
					headers:
						{
						  'Authorization': 'Bearer ' + localStorage.apiToken
					  }
					,
					contentType:'multipart/form-data',
					data:{
					  id :localStorage.idsurat
					},
					success:function(data)
					{
						try
						{
							app.preloader.hide();
							$$("#meta_idsurat").val(localStorage.idsurat);
							var returned = JSON.parse(data);
							//data
							if(returned['data'][0]['nomor_agenda']){$$("#meta_agenda").val(returned['data'][0]['nomor_agenda']);}
							if(returned['data'][0]['asal_naskah']){$$("#meta_naskah").val(returned['data'][0]['asal_naskah']);}
							if(returned['data'][0]['nomor_surat']){$$("#meta_nosurat").val(returned['data'][0]['nomor_surat']);}
							if(returned['data'][0]['tanggal_masuk_surat']){$$("#meta_tglsurat").val(returned['data'][0]['tanggal_masuk_surat']);}
							if(returned['data'][0]['jenis_surat']){$$("#meta_jenis").val(returned['data'][0]['jenis_surat']);}
							if(returned['data'][0]['lampiran']){$$("#meta_lampiran").val(returned['data'][0]['lampiran']);}
							if(returned['data'][0]['perihal']){$$("#meta_perihal").val(returned['data'][0]['perihal']);}
							if(returned['data'][0]['tembusan']){$$("#meta_tembusan").val(returned['data'][0]['tembusan']);}
							//kegiatan
							if(returned['data'][0]['kegiatan']){$$("#meta_namakeg").val(returned['data'][0]['kegiatan']);}
							if(returned['data'][0]['tempat_kegiatan']){$$("#meta_tempatkeg").val(returned['data'][0]['tempat_kegiatan']);}
							if(returned['data'][0]['waktu_kegiatan']){$$("#meta_waktukeg").val(returned['data'][0]['waktu_kegiatan']);}
							if(returned['data'][0]['waktu_kegiatan_end']){$$("#meta_waktukegsel").val(returned['data'][0]['waktu_kegiatan_end']);}
							//pengirim
							if(returned['data'][0]['pengirim_str']){$$("#meta_namapengirim").val(returned['data'][0]['pengirim_str']);}
							if(returned['data'][0]['hp_sender']){$$("#meta_hp").val(returned['data'][0]['hp_sender']);}
							if(returned['data'][0]['email_sender']){$$("#meta_email").val(returned['data'][0]['email_sender']);}
							//kepada
							var appendKepada = "";
							var cleanedKepada = returned['data'][0]['kepada'].replace('[','');
							cleanedKepada = cleanedKepada.replace(']','');

							var arrayKepada = cleanedKepada.split(",");
							for (var i = 0; i < arrayKepada.length ; i++) 
							{
								var listIdUser = JSON.parse(data);
								var urlRequest2 = url+'/api/kotak_masuk/disampaikan_kepada/'+arrayKepada[i];
								app.request({
									url: urlRequest2,
									method:'get',
									headers:
										{
										'Authorization': 'Bearer ' + localStorage.apiToken
									}
									,
									contentType:'multipart/form-data',
									data:{
										
									},
									success:function(data)
									{
										var returned2 = JSON.parse(data);
										appendKepada = "<small>- "+ returned2['data'][0]['nip'] +" - <b>"+ returned2['data'][0]['name'] +"</b> - "+ returned2['data'][0]['jabatan'] +"</small><br>";
										$$("#meta_kepada").append(appendKepada);
									},
									error: function(err) 
									{
	
									}
								})
							}
							// //end for
						}
						catch(err)
						{
							app.preloader.hide();
							app.dialog.alert("Metadata tidak dapat ditampilkan.");
							// window.history.back();
							router.back();
						}
						
					},
					error: function(err) {
						
					  app.dialog.alert("status error : "+err['status']+" error msg : "+err['statusText']+" url : " + urlRequest);
					}
				})

				//detail disposisi
				var urlRequestDisposisi = url+'/api/kotak_masuk/detail_disposisi/' + localStorage.idsurat ;
				app.preloader.show();
				var isi_disposisi ='';
				app.request({
					url: urlRequestDisposisi,
					method:'post',
					async: false,
					headers:
						{
						  'Authorization': 'Bearer ' + localStorage.apiToken
					  }
					,
					contentType:'application/json',
					data:{
						id :localStorage.idsurat
					},
					success:function(data)
					{
						try
						{
							app.preloader.hide();
							var returned = JSON.parse(data);
							for (var i = 0; i < returned['data'].length ; i++) 
							{	
								var timesplit = returned['data'][i]['time'].split(" ");
								var tglfix = timesplit[0].split("-");
								var waktufix = timesplit[1].split(".");
								isi_disposisi = isi_disposisi + 
								'<li class="item-content item-input">\
								<div class="item-inner">\
								<div class="item-title item-label">Tanggal Masuk Surat</div>\
								<div class="item-input-wrap">\
								<input type="text" value="'+ tglfix[2] + '-'+ tglfix[1] + '-' + tglfix[0] + ' ' + waktufix[0]  +'" id="meta_tembusan" readonly>\
								</div>\
								</div>\
								</li>\
								<div class="item-content item-input">\
								<div class="item-inner">\
								<div class="item-input-wrap" style="color:black;font-size: 14px;">\
								Di Disposisikan oleh '+ returned['data'][i]['nip'] +' - '+ returned['data'][i]['name'] +'<br>\
								'+ returned['data'][i]['jabatan'] +'\
								<br>\
								'+ returned['data'][i]['keterangan'] +'\
								<font style="font-weight: bold;">Diteruskan kepada:</font><br>';
								
								//check diteruskan kepada
								var urlRequestDisposisiKepada = url+'/api/kotak_masuk/get_str_kepada_disposisi/';
								app.request({
									url: urlRequestDisposisiKepada,
									method:'post',
									async: false,
									headers:
										{
										  'Authorization': 'Bearer ' + localStorage.apiToken
									  }
									,
									contentType:'application/json',
									data:{
										id : returned['data'][i]['surat_id'],
										id_surat_action: returned['data'][i]['said']
									},
									success:function(data)
									{
										try
										{
											app.preloader.hide();
											var returned2 = JSON.parse(data);
											var isi_kepada ='';
											for (var i = 0; i < returned2['data'].length ; i++) 
											{
												isi_kepada = isi_kepada + (i+1) + ". "+ returned2['data'][i]['jabatan'] +"<br>";
											}
											isi_disposisi = isi_disposisi + isi_kepada + '</div></div></div>';
											
										}
										catch(err)
										{
											app.preloader.hide();
										}
										
									},
									error: function(err) {
									  app.dialog.alert("status error : "+err['status']+" error msg : "+err['statusText']+" url : " + urlRequestDisposisiKepada);
									}
								})
							} //end for
							
							
							
						}
						catch(err)
						{
							app.preloader.hide();
							app.dialog.alert("Detail Disposisi tidak dapat ditampilkan.");
						}
						
					},
					error: function(err) {
					  app.dialog.alert("status error : "+err['status']+" error msg : "+err['statusText']+" url : " + urlRequestDisposisi);
					}
				})

				$$("#meta_disposisi").html(isi_disposisi);
				//komentar
				var urlRequestKomentar = url+'/api/kotak_masuk/tampil_komentar/'+localStorage.idsurat;
				app.preloader.show();
				app.request({
					url: urlRequestKomentar,
					method:'post',
					headers:
						{
						  'Authorization': 'Bearer ' + localStorage.apiToken
					  }
					,
					contentType:'multipart/form-data',
					data:{
					  id :localStorage.idsurat
					},
					success:function(data)
					{
						try
						{
							app.preloader.hide();
							var returned = JSON.parse(data);
							var dataKomentar = '';
							for (var i = 0; i < returned['data'].length ; i++) 
							{
								var timefix = returned['data'][i]['time'].split(".");
								dataKomentar = dataKomentar +
								'<div class="row">\
									<div class="col-30">\
										<div class="content">\
											<a href="/single-post/"><img src="img/person.png" style="border-radius: 50%;"  alt=""></a>\
										</div>\
									</div>\
									<div class="col-70">\
										<div class="content-text">\
											<font style="color:black;font-weight:bold;">'+ returned['data'][i]['name'] +'</font> '+ timefix[0] +'<br>\
											<p style="color:black">'+ returned['data'][i]['comment'] +'</p>\
										</div>\
									</div>\
								</div>';
							}
							$$("#meta_komentar").html(dataKomentar);
						}
						catch(err)
						{
							app.preloader.hide();
							app.dialog.alert("Komentar tidak dapat ditampilkan.");
							// window.history.back();
							router.back();
						}
						
					},
					error: function(err) {
					  app.dialog.alert("status error : "+err['status']+" error msg : "+err['statusText']+" url : " + urlRequestKomentar);
					}
				})
				
			},//end pageinit
		}
	  },
	  {
	    path: '/ds_popupdisposisi/',
		url: './detailsurat_popupdisposisi.html',
		on: {
	    	pageInit: function (e, page) 
	        {
				
				//
				$$("#ds_tanggalSuratInput").val(localStorage.tglsurat);
					var role = localStorage.role;
					if(localStorage.username == "admin")
					{
						role = "superadmin";
					}
					else
					{
						// role = localStorage.u
					}
					var urlRequest = url+"/api/kotak_masuk/get_kepada_disposisi/"+localStorage.kolok+"/"+role+"";
					app.request({
						url: urlRequest,
						method:'get',
						headers:
						{
							'Authorization': 'Bearer ' + localStorage.apiToken
						}
						,
						contentType:'multipart/form-data',
						data:{
						
						},
						success:function(data){
						var returned = JSON.parse(data);
						var data = returned['data'];
						var append = '<option value="">-</option>';
						for (var i = 0; i <= data.length; i++)
						{
							if(data[i] != null)
							{
								append = append + '<option value="'+data[i]['id']+'" data-name = "'+data[i]['jabatan']+'">'+data[i]['name']+' - '+data[i]['jabatan']+'</option>';
							}
						}
						$$("#ds_disampaikankepada").html(append);
						},
						error: function(err) {
						app.dialog.alert('Failed to  get "JENIS_SURAT"');
						}
					})
				//
				var arr = [];
				$$(document).on('click','#ds_tambahBagianPenerima',function(e){
					e.preventDefault();
					e.stopImmediatePropagation();
					
					var valueBagian = $$("#ds_disampaikankepada option:checked").attr('data-name');
					var value = $$("#ds_disampaikankepada").val();
					if(value != "")
					{
						const { length } = arr;
						const id = length + 1;
						const found = arr.some(el => el[0] === value);
							if (!found)
							{
								arr.push([value,valueBagian]);
								var append = '<div class="row"><div class="col-66"><small> - '+valueBagian+'</small></div><div class="col-33 container"> <button class="col button button-small button-fill bg-red ds_hapusPenerima" data-penerima="'+value+'">X</button></div></div><br>';
								$$("#ds_bagianPenerima").append(append);
							}
					}
					
				});
				//
				$$(document).on('click','.ds_hapusPenerima',function(e){
					e.preventDefault();
					e.stopImmediatePropagation();
					var penerima = $$(this).data('penerima');
					var idDihapus = penerima;
					arr = $.grep(arr, function(value)
						{
							if(value[0]!= idDihapus)
							{
								return value;
							}
							
						});
					var append = "";
					for (var i = 0; i < arr.length; i++) 
					{
						// Things[i]
						append = append + '<div class="row"><div class="col-66"><small> - '+arr[i][1]+'</small></div><div class="col-33 container"> <button class="col button button-small button-fill bg-red ds_hapusPenerima" data-penerima="'+arr[i][0]+'">X</button></div></div>';
					}
					$$("#ds_bagianPenerima").html(append);
				});
				
				//
				$$(document).on('click','#ds_kirimDisposisi',function(e){
					e.preventDefault();
					e.stopImmediatePropagation();
					var today = new Date();
					var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
					var tanggalMasukSurat = localStorage.tglsurat;
					var kepada = arr;
					var _keterangan = $$("#ds_keterangan").val();
					_keterangan = _keterangan.replace(/(?:\r\n|\r|\n)/g, '<br>');
					var keteranganfix = "<p>"  + _keterangan + "</p>";
					
					

					var urlRequest = url+"/api/kotak_masuk/tambah_disposisi";
					var insertToArray = "["
					for (var i = 0; i < arr.length; i++) 
					{
						
						// insertToArray.push( parseInt(arr[i][0]) )
						if(i == arr.length-1)
						{
							insertToArray = insertToArray + arr[i][0];
						}
						else
						{
							insertToArray = insertToArray + arr[i][0]+",";
						}
					}
					insertToArray = insertToArray + "]";
					//using form data
					var arrayIsi= [];
					var stringIsi= "[";
					
					$('input:checkbox[name=demo-checkbox]:checked').each(function() 
					{
						arrayIsi.push($(this).val());
					});

					for (var i = 0; i < arrayIsi.length; i++) 
					{
						
						// insertToArray.push( parseInt(arr[i][0]) )
						if(i == arrayIsi.length-1)
						{
							stringIsi = stringIsi + arrayIsi[i];
						}
						else
						{
							stringIsi = stringIsi + arrayIsi[i]+",";
						}
					}
					stringIsi = stringIsi + "]";
					

					
					//
					$.ajax({
						url: urlRequest,
						type: "post",
						headers:
							{
							  'Authorization': 'Bearer ' + localStorage.apiToken
						  	}
						,
						data:
						{
							_token: "ql3OSXpvc6RPMGVHl3ez8BHIY2hqXOTDLGUxaIUx",
							idSurat: localStorage.idsurat,
							user_id: localStorage.user_id,
							tglMasukSurat: tanggalMasukSurat,
							kepada: insertToArray,
							// filename : document.getElementById("ds_file").files[0], //disini
							tglSuratDisposisi: date,
							keterangan: keteranganfix,
							isi: stringIsi
						},
						async:false,
						beforeSend:function(){
							app.preloader.show();
						},
						success: function (data) {
							app.preloader.hide();	
							mainView.router.back();
							app.dialog.alert('Berhasil menambah surat disposisi.');
							
						},
						error: function(jqXHR, textStatus, errorThrown) {
						 	app.preloader.hide();
							app.dialog.alert('tambah surat disposisi gagal');
						  	console.log(errorThrown);
					 	}
				 	});
				})
			},//end pageinit
		}
	  },
	  {
	    path: '/calendar/',
	    url: './calendar.html',
	    on: {
	    	pageInit: function (e, page) 
	        {
	        	var urlRequest2 = url+'/api/kalender/get_all_event';
	        	var kolokDipilih = localStorage.kolok;
		  		app.request({
				  url: urlRequest2,
				  method:'post',
				  headers:
				  	{
			            'Authorization': 'Bearer ' + localStorage.apiToken
			        }
				  ,
				  contentType:'multipart/form-data',
				  data:{
				  	'kolok':kolokDipilih
				  },
				  success:function(data)
				  {
				  	var arr = [];
				  	var returned2 = JSON.parse(data);
				  	var data = returned2['data'];
				  	for (var i = 0; i < data.length; i++)
				  	{
				  		arr.push([data[i]['waktu_kegiatan'].substring(0,10),data[i]['kegiatan']]);
				  	}
				  	// console.log(arr);
				  	arr.sort();
				  	// console.log(arr);
				  	var year = "";
				  	var appendView = '';
				  	for (var i = 0; i < arr.length; i++) 
				  	{
				  		if(year == arr[i][0].substring(0,4))
				  		{

				  		}
				  		else
				  		{
				  			year = arr[i][0].substring(0,4);
				  			appendView = appendView + "<h2>"+year+"</h2><br>";
				  		}
				  		var bulan = "";
				  		if(arr[i][0].substring(5,7) == "01")
				  			bulan = "JAN";
				  		else if(arr[i][0].substring(5,7) == "02")
				  			bulan = "FEB";
				  		else if(arr[i][0].substring(5,7) == "03")
				  			bulan = "MAR";
				  		else if(arr[i][0].substring(5,7) == "04")
				  			bulan = "APR";
				  		else if(arr[i][0].substring(5,7) == "05")
				  			bulan = "MEI";
				  		else if(arr[i][0].substring(5,7) == "06")
				  			bulan = "JUN";
				  		else if(arr[i][0].substring(5,7) == "07")
				  			bulan = "JUL";
				  		else if(arr[i][0].substring(5,7) == "08")
				  			bulan = "AUG";
				  		else if(arr[i][0].substring(5,7) == "09")
				  			bulan = "SEPT";
				  		else if(arr[i][0].substring(5,7) == "10")
				  			bulan = "OCT";
				  		else if(arr[i][0].substring(5,7) == "11")
				  			bulan = "NOV";
				  		else if(arr[i][0].substring(5,7) == "12")
				  			bulan = "DES";

				  		var tanggal = arr[i][0].substring(8,10);
				  		console.log(arr[i][0]);
				  		appendView = appendView + '<div class="timeline-item">'+
								                    '<div class="timeline-item-date">'+tanggal+'<small>'+bulan+'</small></div>'+
								                    '<div class="timeline-item-divider"></div>'+
								                    '<div class="timeline-item-content">'+arr[i][1]+'</div>'+
								                  '</div>';

				  	}
				  	$$("#agenda").html(appendView);
				  },
			      error: function(err) 
			      {

			      }
				})
	        }
	    }
	  },
	  {
	    path: '/detailsurat/:idsurat/:tipe',
	    url: './detailsurat.html',
	    on: {
	        pageInit: function (e, page) 
	        {	
				var idsurat = page.router.currentRoute.params.idsurat;
				localStorage.idsurat = idsurat;
				var tipe = page.router.currentRoute.params.tipe;
				localStorage.tipe = tipe;

				//event pembeda
				if(tipe == "outbox"){
					// document.getElementById("myBtn").disabled = true;
				}
				//fungsi btn popup disposisi 
				$$(document).on('click','.popup-disposisi',function(e){
					e.stopImmediatePropagation();
					e.preventDefault();
					checkAllowDisposisi();
					
				});
				//meta data
				$$(document).on('click','.metadata',function(e){
					e.stopImmediatePropagation();
					e.preventDefault();
					mainView.router.navigate("/ds_metadata/");
				});

				$$(document).on('click','.riwayat',function(e){
					e.stopImmediatePropagation();
					e.preventDefault();
					mainView.router.navigate("/ds_riwayat/");
				});

				$$(document).on('click','#downloadfilesurat1',function(e){
					e.preventDefault();
					e.stopImmediatePropagation();
					var linkdl = localStorage.fixlink1;
					var namafile = localStorage.namafile1;
					console.log(namafile);
					console.log(linkdl);
					downloadFile(linkdl, namafile);
					// document.getElementById('downloadfilesurat1').click();
				});

				//tte 
				$$(document).on('click','.tte',function(e){
					e.stopImmediatePropagation();
					e.preventDefault();
					var ktp =   localStorage.no_ktp;
					var token = btoa(API_TTE_USERNAME + ":" + API_TTE_PASSWORD);
					// var urlRequest = url+'/api/kotak_keluar/tte_sign/';
					//
					var urlRequest = url+'/api/kotak_keluar/tte_check/';
					console.log(urlRequest)
					app.preloader.show();
					app.request({
						url: urlRequest,
						method:'post',
						headers:
						  	{
					            'Authorization': 'Bearer ' + localStorage.apiToken
					        }
						  ,
						contentType:'application/json',
						data:{
							'nik':ktp,
							'_token' : token,
						},
						success:function(data){
							app.preloader.hide();
							var returned = JSON.parse(data);
							if(returned['status_code'] == 1111){
								mainView.router.navigate('/tte_page/');
							}else{
								app.dialog.alert(returned['message']);
							}
						},
						error: function(xhr, status)
						{
							app.preloader.hide();
							app.dialog.alert('Maaf terjadi kesalahan :'+ xhr.response);
							console.log(xhr)
						}
					})
				});
				//fungsi tambahkan dalam popup disposisi
				

	        	$$('#tambahKomentar').on('click', function () {
				  app.dialog.prompt('Tuliskan komentar anda.', function (komentar) {
				    app.dialog.confirm('Simpan komentar " ' + komentar + '" ?', function () 
				    {
						//replace %20 from komentar
						let fixkomentar = komentar.replaceAll('%20', ' ')
						
				    	var urlRequest = url+'/api/kotak_masuk/tambah_komentar';
				    	app.request({
						  url: urlRequest,
						  method:'post',
						  headers:
						  	{
					            'Authorization': 'Bearer ' + localStorage.apiToken
					        }
						  ,
						  contentType:'application/json',
						  data:{
						  	'id':idsurat,
						  	'komen':fixkomentar,
						  	'id_user':localStorage.user_id
						  },
						  success:function(data){
						  	if(data == "success")
						  	{
						  		app.dialog.alert('Komentar berhasil ditambahkan.');
						  	}
						  },
				          error: function(err) 
				          {
				            app.dialog.alert('Gagal menambah komentar. ERROR: '+err);
				            console.log(err);
				          }
						})
				      	
				    });
				  });
				});


	        	
	        	// app.dialog.alert('idsurat : '+idsurat);
	        	if(tipe =="inbox")
	        	{
	        		var urlRequest = url+'/api/kotak_masuk/detail_surat/'+idsurat;
	        	}
	        	else if(tipe == "disposisi")
	        	{
	        		var urlRequest = url+'/api/kotak_masuk/detail_surat/'+idsurat;
	        	}
	        	else if(tipe == "outbox")
	        	{
	        		var urlRequest = url+'/api/kotak_masuk/detail_surat/'+idsurat;
	        	}
	        	
	        	app.request({
				  url: urlRequest,
				  method:'post',
				  headers:
				  	{
			            'Authorization': 'Bearer ' + localStorage.apiToken
			        }
				  ,
				  contentType:'multipart/form-data',
				  data:{
					user_id: localStorage.user_id,
					id :idsurat
				  },
				  success:function(data)
				  {
				  	try
				  	{
						
				  		var returned = JSON.parse(data);
						console.log(returned['data'])
						localStorage.id_dokumen = returned['data'][0]['id_dokumen']; 
						localStorage.file_tte = returned['data'][0]['file_tte'];
						localStorage.id_surat = returned['data'][0]['id_surat'];
						if(localStorage.kolok == returned['data'][0]['satuan_kerja_id'] && returned['data'][0]['stat_verif'] == 1){
							$$(".row-kadis").show();
						}else{
							$$(".row-kadis").hide();
						}
					  	if(!returned['data'][0]['nomor_agenda'])
					  	{

					  	}
					  	else
					  	{
					  		$$(".nomoragenda").val(returned['data'][0]['nomor_agenda']);
					  	}
					  	
					  	if(!returned['data'][0]['nomor_surat'])
					  	{

					  	}
					  	else
					  	{
					  		$$(".nosurat").val(returned['data'][0]['nomor_surat']);
					  	}
					  	
					  	$$(".kodeindex").val(returned['data'][0]['kode_index']);
					  	$$(".tanggalsurat").val(returned['data'][0]['tanggal_masuk_surat']);
					  	$$(".lampiran").val(returned['data'][0]['lampiran']);
					  	$$(".perihal").val(returned['data'][0]['perihal']);
					  	$$(".tembusan").val(returned['data'][0]['tembusan']);

					  	var appendKepada = "";
					  	var cleanedKepada = returned['data'][0]['kepada'].replace('[','');
					  	cleanedKepada = cleanedKepada.replace(']','');
					  	// console.log(cleanedKepada);
					  	var arrayKepada = cleanedKepada.split(",");
					  	// console.log("kepada : " + returned['data'][0]['kepada']);
					  	// console.log("panjang array : " + returned['data'][0]['kepada'].length);
					  	for (var i = 0; i < arrayKepada.length ; i++) 
					  	{
					  		var listIdUser = JSON.parse(data);
					  		var urlRequest2 = url+'/api/kotak_masuk/disampaikan_kepada/'+arrayKepada[i];
					  		console.log(urlRequest2);
					  		app.request({
							  url: urlRequest2,
							  method:'get',
							  async: false,
							  headers:
							  	{
						            'Authorization': 'Bearer ' + localStorage.apiToken
						        }
							  ,
							  contentType:'multipart/form-data',
							  data:{
							  	
							  },
							  success:function(data)
							  {
							  	var returned2 = JSON.parse(data);
							  	appendKepada = "<small>- "+ returned2['data'][0]['nip'] +" - <b>"+ returned2['data'][0]['name'] +"</b> - "+ returned2['data'][0]['jabatan'] +"</small><br>";
							  	$$("#listDisampaikan").append(appendKepada);
							  },
						      error: function(err) 
						      {

						      }
							})
						}
						let contentFile = '<p class="cdark">File : - ';
						if(returned['data'][0]['fileupload'] != "[]" && returned['data'][0]['fileupload']){
							var filename = returned['data'][0]['fileupload'];
							if(returned['data'][0]['fileupload'].includes("[")){
								var array = JSON.parse(returned['data'][0]['fileupload']);
								filename = array[0];
							}
							var linkencode = filename.replaceAll(" ", "%20");
							var fixlink = urlfile + "/" + linkencode;
							contentFile = contentFile + '<br><a class="link external clink" href="'+ fixlink + '">File 1</a>';
						}
						if(returned['data'][0]['fileupload2'] != "[]" && returned['data'][0]['fileupload2']){
							var filename = returned['data'][0]['fileupload2'];
							if(returned['data'][0]['fileupload2'].includes("[")){
								var array = JSON.parse(returned['data'][0]['fileupload2']);
								filename = array[0];
							}
							var linkencode = filename.replaceAll(" ", "%20");
							var fixlink = urlfile + "/" + linkencode;
							contentFile = contentFile + '<br><a class="link external clink" href="'+ fixlink + '">File 2</a>';
						}
						if(returned['data'][0]['fileupload3']  && returned['data'][0]['fileupload3']){
							var filename = returned['data'][0]['fileupload3'];
							if(returned['data'][0]['fileupload3'].includes("[")){
								var array = JSON.parse(returned['data'][0]['fileupload3']);
								filename = array[0];
							}
							var linkencode = filename.replaceAll(" ", "%20");
							var fixlink = urlfile + "/" + linkencode;
							contentFile = contentFile + '<br><a class="link external clink" href="'+ fixlink + '">File 3</a>';
						}
						if(returned['data'][0]['fileupload4']  && returned['data'][0]['fileupload4']){
							var filename = returned['data'][0]['fileupload4'];
							if(returned['data'][0]['fileupload4'].includes("[")){
								var array = JSON.parse(returned['data'][0]['fileupload4']);
								filename = array[0];
							}
							var linkencode = filename.replaceAll(" ", "%20");
							var fixlink = urlfile + "/" + linkencode;
							contentFile = contentFile + '<br><a class="link external clink" href="'+ fixlink + '">File 4</a>';
						}
						if(returned['data'][0]['fileupload5']  && returned['data'][0]['fileupload5']){
							var filename = returned['data'][0]['fileupload5'];
							if(returned['data'][0]['fileupload5'].includes("[")){
								var array = JSON.parse(returned['data'][0]['fileupload5']);
								filename = array[0];
							}
							var linkencode = filename.replaceAll(" ", "%20");
							var fixlink = urlfile + "/" + linkencode;
							contentFile = contentFile + '<br><a class="link external clink" href="'+ fixlink + '">File 5</a>';
						}
						contentFile = contentFile + '</p>';

						  $$("#listDisampaikan").append(contentFile);
				  	}
				  	catch(err)
				  	{
						app.preloader.hide();
						console.log(err)
				  		app.dialog.alert("Detail surat tidak dapat ditampilkan.");
				  		// window.history.back();
				  		router.back();
				  	}
				  	
				  	// console.log(appendKepada);
				  	// $$("#listDisampaikan").html(appendKepada);
				  	// console.log(returned['data']);
				  },
		          error: function(err) {
					app.preloader.hide();
		            console.log("status error : "+err['status']+" error msg : "+err['statusText']+" url : " + urlRequest);
		            app.dialog.alert("status error : "+err['status']+" error msg : "+err['statusText']+" url : " + urlRequest);
		          }
				})
	        },
	      }
	  },
	  {
	    path: '/detailsuratkeluar/',
	    url: './detailsuratkeluar.html',
	  },
	  {
	    path: '/inputdisposisi/',
	    url: './inputdisposisi.html',
	  },
	  {
	    path: '/login/',
	    url: './login.html',
	  },
	  {
	    path: '/home/',
	    url: './home.html',
	  }

	  
	],
	view: {
		pushState: true,
	},
	textEditor: {
	    buttons: ['bold', 'italic'],
	}
});





var mainView = app.views.create('.view-main');




function ubahNama(nama,inisial)
{
	$$("#namaUserLogin").html(nama);
	$$("#kolokUserLogin").html(inisial);
}

function hapusPenerima(id) {
  console.log(id)
}

$$("#hapusPenerima").on("click",function(){
	console.log("hahaha");
})

$$(document).on('page:init', function (e, page)
{
	var today = new Date();
		var pickerInline = app.picker.create({
		  containerEl: '#demo-picker-date-container',
		  inputEl: '#demo-picker-date',
		  toolbar: false,
		  rotateEffect: true,
		  value: [
		    today.getMonth(),
		    today.getDate(),
		    today.getFullYear(),
		    today.getHours(),
		    today.getMinutes() < 10 ? '0' + today.getMinutes() : today.getMinutes()
		  ],
		  formatValue: function (values, displayValues) {
		    return displayValues[0] + ' ' + values[1] + ', ' + values[2] + ' ' + values[3] + ':' + values[4];
		  },
		  cols: [
		    // Months
		    {
		      values: ('0 1 2 3 4 5 6 7 8 9 10 11').split(' '),
		      displayValues: ('January February March April May June July August September October November December').split(' '),
		      textAlign: 'left'
		    },
		    // Days
		    {
		      values: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],
		    },
		    // Years
		    {
		      values: (function () {
		        var arr = [];
		        for (var i = 1950; i <= 2030; i++) { arr.push(i); }
		          return arr;
		      })(),
		    },
		    // Space divider
		    {
		      divider: true,
		      content: '&nbsp;&nbsp;'
		    },
		    // Hours
		    {
		      values: (function () {
		        var arr = [];
		        for (var i = 0; i <= 23; i++) { arr.push(i); }
		          return arr;
		      })(),
		    },
		    // Divider
		    {
		      divider: true,
		      content: ':'
		    },
		    // Minutes
		    {
		      values: (function () {
		        var arr = [];
		        for (var i = 0; i <= 59; i++) { arr.push(i < 10 ? '0' + i : i); }
		          return arr;
		      })(),
		    }
		  ],
		  on: {
		    change: function (picker, values, displayValues) {
		      var daysInMonth = new Date(picker.value[2], picker.value[0]*1 + 1, 0).getDate();
		      if (values[1] > daysInMonth) {
		        picker.cols[1].setValue(daysInMonth);
		      }
		    },
		  }
		});

		var pickerInline2 = app.picker.create({
		  containerEl: '#demo-picker-date-container2',
		  inputEl: '#demo-picker-date2',
		  toolbar: false,
		  rotateEffect: true,
		  value: [
		    today.getMonth(),
		    today.getDate(),
		    today.getFullYear(),
		    today.getHours(),
		    today.getMinutes() < 10 ? '0' + today.getMinutes() : today.getMinutes()
		  ],
		  formatValue: function (values, displayValues) {
		    return displayValues[0] + ' ' + values[1] + ', ' + values[2] + ' ' + values[3] + ':' + values[4];
		  },
		  cols: [
		    // Months
		    {
		      values: ('0 1 2 3 4 5 6 7 8 9 10 11').split(' '),
		      displayValues: ('January February March April May June July August September October November December').split(' '),
		      textAlign: 'left'
		    },
		    // Days
		    {
		      values: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],
		    },
		    // Years
		    {
		      values: (function () {
		        var arr = [];
		        for (var i = 1950; i <= 2030; i++) { arr.push(i); }
		          return arr;
		      })(),
		    },
		    // Space divider
		    {
		      divider: true,
		      content: '&nbsp;&nbsp;'
		    },
		    // Hours
		    {
		      values: (function () {
		        var arr = [];
		        for (var i = 0; i <= 23; i++) { arr.push(i); }
		          return arr;
		      })(),
		    },
		    // Divider
		    {
		      divider: true,
		      content: ':'
		    },
		    // Minutes
		    {
		      values: (function () {
		        var arr = [];
		        for (var i = 0; i <= 59; i++) { arr.push(i < 10 ? '0' + i : i); }
		          return arr;
		      })(),
		    }
		  ],
		  on: {
		    change: function (picker, values, displayValues) {
		      var daysInMonth = new Date(picker.value[2], picker.value[0]*1 + 1, 0).getDate();
		      if (values[1] > daysInMonth) {
		        picker.cols[1].setValue(daysInMonth);
		      }
		    },
		  }
		});
	if(page.name == "detailoutbox")
	{
		$$('.open-confirm').on('click', function () {
		  app.dialog.confirm('Apakah anda yakin untuk menghapus surat ini ?', function () {
		    app.dialog.alert('Pesan telah berhasil dihapus.');
		    page.router.back('/outbox/');
		  });
		});
	}
	if(page.name == "menu")
	{
		$$("#logoutBtn").on('click',function(){
			app.dialog.confirm('Apakah anda yakin ingin Log Out dari E-Surat ?', function () {
			    localStorage.removeItem("user_id");
			    localStorage.removeItem("username");
			    localStorage.removeItem("id_user");
			    localStorage.removeItem("nama");
			    localStorage.removeItem("npwpd");
			    localStorage.removeItem("namaPerusahaan");
				localStorage.removeItem("apiToken");
				localStorage.removeItem("id_dokumen");
				localStorage.removeItem("file_tte");
				localStorage.removeItem("id_surat");
				localStorage.clear();
				mainView.router.navigate('/');
				
			});
		});
	}
	if(page.name == "inbox")
	{
			
			var kolokDipilih = localStorage.kolok;
			var urlRequest = url+'/api/inbox/tabel_inbox/'+localStorage.user_id;
			// var urlRequest = "10.212.24.89/gg/test.php";
			var role = "";
			if(localStorage.username == "admin")
			{
				role = "superadmin";
			}


            
			var fd = new FormData();    
			fd.append( 'role', role );
			fd.append( 'kolok', kolokDipilih );

			if(localStorage.kolok == "nofilter"){
				app.dialog.alert('Mohon Pilih Kolok Terlebih Dahulu');
			}else{
				app.preloader.show();
				app.request({
					url: urlRequest,
					method:'post',
					headers:
						{
						  'Authorization': 'Bearer ' + localStorage.apiToken
					  }
					,
					contentType:'multipart/form-data',
					data:{
						'role':localStorage.role,
						'kolok':localStorage.kolok
					},
					success:function(data){
						var returned = JSON.parse(data);
						var dataKotakMasuk = returned['data'];
						var appendToView = "<h3>Data Surat Masuk : </h3>";
						for (var i = 0; i < dataKotakMasuk.length; i++) 
						{
							var bgcard = 'bgunread';
							var arrayRead = JSON.parse(dataKotakMasuk[i]['read_by']);
							$.each(arrayRead, function(key,value){
								if(value == localStorage.user_id){
									bgcard = '';
								}
							});
							appendToView = appendToView + '<div class="card '+ bgcard+'"><div class="card-content" style="margin: 10px;"><a href="/detailsurat/'+dataKotakMasuk[i]['id']+'/inbox/"><table><tr><td>'+dataKotakMasuk[i]['nomor_surat']+' <b>('+dataKotakMasuk[i]['tanggal_masuk_surat']+')</b></td></tr><tr><td><b>Nomor Agenda :</b>'+dataKotakMasuk[i]['nomor_agenda']+'</td></tr><tr><td><b>Lampiran :</b> '+ dataKotakMasuk[i]['lampiran'] +' Lembar</td></tr><tr><td><b>Perihal :</b> '+dataKotakMasuk[i]['perihal']+'</td></tr></table></a></div></div><br>';
							// 
						}
	  
						var jumlahData = dataKotakMasuk.length;
						if (jumlahData < 1)
						{
							appendToView = "<h4>Tidak ada surat masuk.</h4>";
						}
						$$("#kotaksuratmasuk").html(appendToView);
						// console.log(dataKotakMasuk);
						app.preloader.hide();
					},
					error: function(err) {
						app.preloader.hide();
						app.dialog.alert('Failed to get "KOTAK MASUK" ');
						console.log(err);
					}
				})
			}
			
	}
	if(page.name == "carisurat")
	{
		$$("#cariSurat").on('click',function(){
			console.log("cari surat");
			var appendToView = "";
			
			var tipesurat = $$("#tipesurat").val();
			var tanggalmasuksurat = $$("#tanggalmasuksurat").val();
			var tanggalsurat = $$("#tanggalsurat").val();
			var nomorsurat = $$("#nomorsurat").val();
			var lampiran = $$("#lampiran").val();
			var perihal = $$("#perihal").val();
			var tembusan = $$("#tembusan").val();
			var namapengirim = $$("#namapengirim").val();
			var nomorhp = $$("#nomorhp").val();
			var email = $$("#email").val();
			var namaacara = $$("#namaacara").val();
			var tempatacara = $$("#tempatacara").val();
			var nomordisposisi = $$("#nomordisposisi").val();
			var kolokDipilih = localStorage.kolok;

			var urlRequest = url+'/api/cari_surat';
			// var urlRequest = "10.212.24.89/gg/test.php";
			
			
			app.request({
			  url: urlRequest,
			  method:'post',
			  headers:
			  	{
		            'Authorization': 'Bearer ' + localStorage.apiToken
		        }
			  ,
			  contentType:'multipart/form-data',
			  data:{
			  	'nosurat':role,
			  	'tglsurat':tanggalsurat,
			  	'lampiran':lampiran,
			  	'perihal':perihal,
			  	'tembusan':tembusan,
			  	'namaAcara':namaacara,
			  	'waktu_kegiatan':"",
			  	'jamMulai':"",
			  	'waktu_kegiatan_end':"",
			  	'namaPengirim':namapengirim,
			  	'noHP':nomorhp,
			  	'mail':email,
			  	'nodisposisi':nomordisposisi,
			  	'kolok':kolokDipilih,

			  },
			  success:function(data){
			  	var returned = JSON.parse(data);
			  	var dataKotakMasuk = returned['data'];
			  	console.log(dataKotakMasuk);
			  	for (var i = 0; i <= dataKotakMasuk.length-1; i++) 
			  	{
			  		if(dataKotakMasuk != null)
			  		{
			  			appendToView = appendToView + '<div class="card"><div class="card-content" style="margin: 10px;"><a><table><tr><td>'+dataKotakMasuk[i]['nomor_surat']+' <b>('+dataKotakMasuk[i]['tanggal_masuk_surat']+')</b></td></tr><tr><td><b>Nomor Agenda :</b>'+dataKotakMasuk[i]['nomor_agenda']+'</td></tr><tr><td><b>Lampiran :</b> '+ dataKotakMasuk[i]['lampiran'] +' Lembar</td></tr><tr><td><b>Perihal :</b> '+dataKotakMasuk[i]['perihal']+'</td></tr></table></a></div></div><br>';
			  		}
			  		
			  		// 
			  	}
			  	var dynamicPopup = app.popup.create({
				  content: '<div class="popup">'+
							  '<div class="page-content"><div class="block">'+
				                '<a class="link popup-close">'+
				                    '<i class="f7-icons">arrow_left</i><h3>Kembali</h3>'+
				                '</a>'+
				              '</div><div class="container">'+
				                '<div class="row">'+
				                    '<div class="col-100">'+
				                        '<h3><u>Hasil Pencarian :</u></h3><br> <div id="hasilcarisurat"></div>'+
						               
				                    '</div>'+
				                '</div></div>'+
						            
				            '</div></div>',
				  // Events
				  on: {
				    open: function (popup)
				    {
				    	// console.log("hasil");

				    	$$("#hasilcarisurat").html(appendToView);

				    },
				    opened: function (popup) {
				      console.log('Popup opened');
				    },
				  }
				});
			  	// $$("#kotaksuratmasuk").html(appendToView);
			  	// console.log(dataKotakMasuk);
			  	app.preloader.hide();
			  	dynamicPopup.open();
			  },
	          error: function(err) {
	            // app.dialog.alert('Failed to get "KOTAK MASUK" ');
	            console.log(err);
	          }
			})

			
		})
	}
	if(page.name == "home")
	{
		var totalKotakMasuk = 0;
		var totalKotakKeluar = 0;
		var totalDisposisi = 0;

		if(localStorage.username == "admin")
		{
			$$("#namaUserLogin").html("ADMIN");
			$$("#kolokUserLogin").html("Administrator E-SURAT Madiun");
			if(!localStorage.kolok)
			{

			}
			else
			{
				$$("#selectKolok").val(localStorage.kolok);
				$$("#jumlahOutbox").html(localStorage.outboxTotal);
				$$("#jumlahDisposisi").html(localStorage.disposisiTotal);
				$$("#jumlahInbox").html(localStorage.inboxTotal);
			}
			
		}
		else
		{
			$$("#selectKolok").val(localStorage.kolok);
			var textOption =  $("#selectKolok option:selected").text();
			var valueOption = $("#selectKolok option:selected").val();
			var overwriteOption = '<option value="'+ valueOption +'">'+ textOption +'</option>';
			$$("#selectKolok").html(overwriteOption);
			$$("#namaUserLogin").html(localStorage.nama);
			$$("#kolokUserLogin").html(localStorage.role);
			
			
		}

		//call function directly if KOLOK undefined
		if(localStorage.kolok != "nofilter"){
			setTimeout(function(){ homePageGetDashboardData(); }, 1500);
			
		}
		//check if admin or no
		
		
		$$(document).on('click','.refresh-page',function(e){
			e.preventDefault();
			e.stopImmediatePropagation();
			mainView.router.refreshPage();
		});

		$$("#selectKolok").on('change',function()
		{
			var kolokDipilih = $$("#selectKolok").val();
			localStorage.kolok = kolokDipilih;

			if(localStorage.kolok == "nofilter"){
				$$("#jumlahInbox").html('0');
				$$("#jumlahDisposisi").html('0');
				$$("#jumlahOutbox").html('0');
			}
			else{
				homePageGetDashboardData(); //global function
			}

			
		}) //end onchange func

		var btnDynamic = app.actions.create({
			buttons: [
				{
				text: 'Surat Masuk',
				bold: true,
				color:'blue',
				onClick: function () {
					localStorage.stateSurat = "Masuk";
					mainView.router.navigate('/compose/');
				},
				},
				{
				text: 'Surat Keluar',
				bold: true,
				color:'blue',
				onClick: function () {
					localStorage.stateSurat = "Keluar";
					mainView.router.navigate('/compose/');
				},
				},
				{
				text: 'Kembali',
				color: 'red'
				},
			]
		});
		$$(document).on('click','#btnTambahSurat',function(e){
			e.preventDefault();
			e.stopImmediatePropagation();
			btnDynamic.open();
		});	
	}
	if(page.name == "index")
	{	
		checkLogin();
		$$(document).on('click','#btnLogin',function(e){
			e.preventDefault();
			e.stopImmediatePropagation();
			var usernameInput = $$("#username").val();
			var passwordInput = $$("#password").val();

			// app.request.setup({
			//   headers: {
			//     'Content-Type': 'application/json',
			//     'Accept':'application/json'
			//   }
			// })
			app.preloader.show();
			var urlRequest = url+"/api/login";
			console.log(urlRequest);
			app.request.post(
				urlRequest,
				{
					username: usernameInput,
					password: passwordInput,
				},
				function(data)
				{
					var returned = JSON.parse(data);
					console.log(data);
					app.dialog.alert('Login Berhasil.');
					localStorage.user_id = returned['data']['user_id'];
					localStorage.username = returned['data']['username'];
					localStorage.id_user = returned['data']['ID_USER'];
					localStorage.nama = returned['data']['name'];
					localStorage.apiToken = returned['data']['token'];
					localStorage.role = returned['data']['role_id'];
					localStorage.satuankerja = returned['data']['satuan_kerja_id'];
					localStorage.no_ktp = returned['data']['no_ktp'];
					if(localStorage.role == 'superadmin'){
						localStorage.kolok = "nofilter";
					}else{
						localStorage.kolok = returned['data']['satuan_kerja_id'];
					}
					app.preloader.show();
					mainView.router.navigate("/home/");
					
				},
				function(error)
				{	
					app.preloader.hide();
					app.dialog.alert('Login Gagal.\nPeriksa Username atau Password anda !');
				});
		});
	}
	if(page.name == "outbox")
	{
			
			var kolokDipilih = localStorage.kolok;
			var urlRequest = url+'/api/kotak_keluar/div_kotak_keluar/'+localStorage.user_id;
			// var urlRequest = "10.212.24.89/gg/test.php";
			let contentDiverifikasi = contentRevisi = contentDisetujui = "";
			var role = "";
			if(localStorage.username == "admin")
			{
				role = "superadmin";
			}

			if(localStorage.kolok == "nofilter"){
				app.dialog.alert('Mohon Pilih Kolok Terlebih Dahulu');
			}else{
				app.preloader.show();
				app.request({
					url: urlRequest,
					method:'post',
					headers:
						{
						  'Authorization': 'Bearer ' + localStorage.apiToken
					  }
					,
					contentType:'multipart/form-data',
					data:{
						'role':role,
						'kolok':kolokDipilih
					},
					success:function(data){
						contentDiverifikasi = contentRevisi = contentDisetujui = "";
						var returned = JSON.parse(data);
						var dataOutbox = returned['data'];
						var appendToView = "<h3>Data Surat Keluar : </h3>";
						if(dataOutbox != null){
								for (var i = 0; i <= dataOutbox.length-1; i++) 
								{
									//check read
									var bgcard = 'bgunread';
									var arrayRead = JSON.parse(dataOutbox[i]['read_by']);
									$.each(arrayRead, function(key,value){
										if(value == localStorage.user_id){
											bgcard = '';
										}
									});

									if (!dataOutbox[i]['is_revisi'] && !dataOutbox[i]['stat_verif']){
										contentDiverifikasi = contentDiverifikasi + '<div class="card '+bgcard+'"><div class="card-content" style="margin: 10px;"><a href="/detailsurat/'+dataOutbox[i]['id']+'/outbox/"><table><tr><td>'+dataOutbox[i]['nomor_surat']+' <b>('+dataOutbox[i]['tanggal_masuk_surat']+')</b></td></tr><tr><td><b>Nomor Agenda :</b>'+dataOutbox[i]['nomor_agenda']+'</td></tr><tr><td><b>Lampiran :</b> '+ dataOutbox[i]['lampiran'] +' Lembar</td></tr><tr><td><b>Perihal :</b> '+dataOutbox[i]['perihal']+'</td></tr></table></a></div></div><br>';
									}
									else if(dataOutbox[i]['stat_verif'] === 0){
										contentRevisi = contentRevisi + '<div class="card '+bgcard+'"><div class="card-content" style="margin: 10px;"><a href="/detailsurat/'+dataOutbox[i]['id']+'/outbox/"><table><tr><td>'+dataOutbox[i]['nomor_surat']+' <b>('+dataOutbox[i]['tanggal_masuk_surat']+')</b></td></tr><tr><td><b>Nomor Agenda :</b>'+dataOutbox[i]['nomor_agenda']+'</td></tr><tr><td><b>Lampiran :</b> '+ dataOutbox[i]['lampiran'] +' Lembar</td></tr><tr><td><b>Perihal :</b> '+dataOutbox[i]['perihal']+'</td></tr></table></a></div></div><br>';
									}
									else if(dataOutbox[i]['stat_verif'] === 1){
										contentDisetujui = contentDisetujui + '<div class="card '+bgcard+'"><div class="card-content" style="margin: 10px;"><a href="/detailsurat/'+dataOutbox[i]['id']+'/outbox/"><table><tr><td>'+dataOutbox[i]['nomor_surat']+' <b>('+dataOutbox[i]['tanggal_masuk_surat']+')</b></td></tr><tr><td><b>Nomor Agenda :</b>'+dataOutbox[i]['nomor_agenda']+'</td></tr><tr><td><b>Lampiran :</b> '+ dataOutbox[i]['lampiran'] +' Lembar</td></tr><tr><td><b>Perihal :</b> '+dataOutbox[i]['perihal']+'</td></tr></table></a></div></div><br>';
									}	
									
								}
							// 
						}
						var jumlahData = dataOutbox.length;
						if (contentDiverifikasi == ""){
							contentDiverifikasi = "<br><h4>Tidak ada data ditampilkan</h4>";
						}
						if (contentRevisi == ""){
							contentRevisi = "<br><h4>Tidak ada data ditampilkan</h4>";
						}
						if (contentDisetujui == ""){
							contentDisetujui = "<br><h4>Tidak ada data ditampilkan</h4>";
						}
						$$("#kotaksuratkeluar").html(contentDiverifikasi);
						app.preloader.hide();
					},
					error: function(err) {
					  app.preloader.hide();
					  console.log(err);
					}
				})
			}

			$$(document).on('click','.chip-jenis',function(e){
				e.preventDefault();
				e.stopImmediatePropagation();
				$$('.chip-jenis').removeClass("color-green");
				$$(this).addClass("color-green")
				var jenis = $$(this).data('jenis');
				var content = "";
				if(jenis == "verifikasi"){
					content = contentDiverifikasi;
				}
				else if(jenis == "revisi"){
					content = contentRevisi;
				}
				else if(jenis == "disetujui"){
					content = contentDisetujui;
				}

				$$("#kotaksuratkeluar").html(content);
			});
			
	}
	if(page.name == "inputdisposisi")
	{
		var arr = [];
		$$("#tambahBagianPenerima").on('click',function(){
			var valueBagian = $$("#disampaikankepada option:checked").attr('data-name');
			var value = $$("#disampaikankepada").val();
			console.log(valueBagian);
			// $$("#bagianPenerima").append("<li> - "+valueBagian+"</li>");
			  const { length } = arr;
			  const id = length + 1;
			  const found = arr.some(el => el === value);
			  	if (!found)
			  	{

			  		arr.push(value);
			  		
                                     
                                  
			  		// var append = '<p><div class="item-inner"><div class="item-title"> - '+ valueBagian+'</div></div></p>';
			  		var append = '<div class="row"><div class="col-66"><small> - '+valueBagian+'</small></div><div class="col-33"> <button href="" class="col button button-small button-fill bg-red" id="tambahBagianPenerima">X</button></div></div>';
					$$("#bagianPenerima").append(append);
			  	}
		})
	}
	if(page.name == "compose")
	{
		arrTembusan = [];
		arrKepada  = [];
		$(".statesurat").html(localStorage.stateSurat)
		if(localStorage.stateSurat == "Masuk"){
			$(".stateMasuk").show();
			$(".stateKeluar").hide();
			var role = localStorage.role;
			if(localStorage.username == "admin")
			{
				role = "superadmin";
			}
			else
			{
				// role = localStorage.u
			}
			var urlRequest = url+"/api/kotak_masuk/get_kepada_surat/"+localStorage.kolok+"/"+role+"";
			app.request({
				url: urlRequest,
				method:'get',
				headers:
				{
					'Authorization': 'Bearer ' + localStorage.apiToken
				}
				,
				contentType:'multipart/form-data',
				data:{
				
				},
				success:function(data){
				var returned = JSON.parse(data);
				var data = returned['data'];
				// console.log(data); // console disampaikan kepada
				var append = '<option value="">-</option>';
				for (var i = 0; i <= data.length; i++)
				{
					if(data[i] != null)
					{
						// console.log(data[i]);
						// append = append + '<option value="'+data[i]['id']+'">'+data[i]['type_name']+'</option>';
						append = append + '<option value="'+data[i]['id']+'" data-name = "'+data[i]['jabatan']+'">'+data[i]['name']+' - '+data[i]['jabatan']+'</option>';
					}
					// append = append + '<option value="'+data[i]['satuan_kerja_id']+'">'+data[i]['satuan_kerja_nama']+'</option>';
				}
					$$("#disampaikankepadacompose").html(append);
					$$("#tembusanCompose").html(append);
				},
				error: function(err) {
				app.dialog.alert('Failed to  get "JENIS_SURAT"');
				}
			})
		} //end if masuk
		if(localStorage.stateSurat == "Keluar"){
			$(".stateMasuk").hide();
			$(".stateKeluar").show();
			var role = localStorage.role;
			if(localStorage.username == "admin")
			{
				role = "superadmin";
			}
			else
			{
				// role = localStorage.u
			}
			var urlRequest = url+"/api/kotak_masuk/get_kepada_surat_keluar";
			app.request({
				url: urlRequest,
				method:'get',
				headers:
				{
					'Authorization': 'Bearer ' + localStorage.apiToken
				}
				,
				contentType:'multipart/form-data',
				data:{
				
				},
				success:function(data){
				var returned = JSON.parse(data);
				var data = returned['data'];
				// console.log(data); // console disampaikan kepada
				var append = '<option value="">-</option>';
				for (var i = 0; i <= data.length; i++)
				{
					if(data[i] != null)
					{
						// console.log(data[i]);
						// append = append + '<option value="'+data[i]['id']+'">'+data[i]['type_name']+'</option>';
						append = append + '<option value="'+data[i]['id']+'" data-name = "'+data[i]['jabatan']+'">'+data[i]['name']+' - '+data[i]['jabatan']+'</option>';
					}
					// append = append + '<option value="'+data[i]['satuan_kerja_id']+'">'+data[i]['satuan_kerja_nama']+'</option>';
				}
					$$("#disampaikankepadacompose").html(append);
					$$("#tembusanCompose").html(append);
				},
				error: function(err) {
				app.dialog.alert('Failed to  get "JENIS_SURAT"');
				}
			})
				
		}
		//isi field jenis surat
		isiDataComposeSurat();

		$$("#masterDataInput").hide();
		$$("#checkboxMasterData").on('change',function()
		{
			if ($$('#checkboxMasterData').is(":checked"))
			{
					$$("#masterDataInput").show();
			}
			else
			{
				$$("#masterDataInput").hide();
			}
			
		});

		$$("#tambahAcaraInput").hide();

		$$("#checkboxTambahAcara").on('change',function()
		{
			if ($$('#checkboxTambahAcara').is(":checked"))
			{
			  	$$("#tambahAcaraInput").show();
			}
			else
			{
				$$("#tambahAcaraInput").hide();
			}
			
		});

		$$("#tipeSurat").on("change",function(){
			var valueTipe = $$("#tipeSurat").val();
			console.log(valueTipe);
			if(valueTipe == "suratmasuk")
			{
				
				
			}
			else if(valueTipe == "suratkeluar")
			{
				var urlRequest = url+"/api/kotak_masuk/get_jenis_surat_keluar";
				app.request({
				  url: urlRequest,
				  method:'post',
				  headers:
				  	{
			            'Authorization': 'Bearer ' + localStorage.apiToken
			        }
				  ,
				  contentType:'multipart/form-data',
				  data:{
				  	
				  },
				  success:function(data){
				  	var returned = JSON.parse(data);
				  	var data = returned['data'];
				  	var append = '<option value="nofilter">-</option>';
				  	for (var i = 0; i <= data.length; i++)
				  	{
				  		if(data[i] != null)
				  		{
				  			append = append + '<option value="'+data[i]['id']+'">'+data[i]['type_name']+'</option>';
				  		}
				  	}
				  	$$("#selectJenisSurat").html(append);
				  },
			      error: function(err) {
			        app.dialog.alert('Failed to  get "JENIS_SURAT"');
			      }
				})

				var urlRequest = url+"/api/kotak_masuk/get_kepada_surat_keluar";
				app.request({
				  url: urlRequest,
				  method:'get',
				  headers:
				  	{
			            'Authorization': 'Bearer ' + localStorage.apiToken
			        }
				  ,
				  contentType:'multipart/form-data',
				  data:{
				  	
				  },
				  success:function(data){
				  	var returned = JSON.parse(data);
				  	var data = returned['data'];
				  	console.log(data);
				  	var append = '<option value="">-</option>';
				  	for (var i = 0; i <= data.length; i++)
				  	{
				  		if(data[i] != null)
				  		{
				  			append = append + '<option value="'+data[i]['id']+'" data-name = "'+data[i]['jabatan']+'">'+data[i]['name']+' - '+data[i]['jabatan']+'</option>';
				  		}
				  	}
				  	$$("#disampaikankepada").html(append);
				  },
			      error: function(err) {
			        app.dialog.alert('Failed to  get "JENIS_SURAT"');
			      }
				})
			}
		})
		
		

		$$(document).on('click','#simpanSurat',function(e){
			e.preventDefault();
			e.stopImmediatePropagation();
			if(localStorage.stateSurat == "Masuk"){
				tambahSuratMasuk();
			}else{
				tambahSuratKeluar();
			}
		});


		

		$$(document).on('click','#tambahBagianPenerimaCompose',function(e){
			e.preventDefault();
			e.stopImmediatePropagation();
			var valueBagian = $$("#disampaikankepadacompose option:checked").attr('data-name');
			var value = $$("#disampaikankepadacompose").val();
			if(value != "")
			{
				// console.log(valueBagian);
			// $$("#bagianPenerima").append("<li> - "+valueBagian+"</li>");
			  const { length } = arrKepada;
			  const id = length + 1;
			  const found = arrKepada.some(el => el[0] === value);
			  	if (!found)
			  	{

					arrKepada.push([value,valueBagian]);
					console.log(arrKepada);
			  		var append = '<div class="row"><div class="col-66"><small> - '+valueBagian+'</small></div><div class="col-33 container"> <button class="col button button-small button-fill bg-red" data-id="'+value+'" id="hapusPenerimaKepada">X</button></div></div>';
					$$("#bagianPenerimaCompose").append(append);
			  	}
			}
			
		})

		$$(document).on('click','#hapusPenerimaKepada',function(e){
			e.preventDefault();
			e.stopImmediatePropagation();
			var idPenerima = $$(this).data('id');
		    	// console.log(arr);
		        var idDihapus = idPenerima
		        arrKepada = $.grep(arrKepada, function(value)
		        	{
		        		if(value[0]!= idDihapus)
		        		{
		        			return value;
		        		}
					});
		        var append = "";
		        for (var i = 0; i < arrKepada.length; i++) 
		        {
		        	append = append + '<div class="row"><div class="col-66"><small> - '+arrKepada[i][1]+'</small></div><div class="col-33 containerr"> <button class="col button button-small button-fill bg-red" data-id="'+arrKepada[i][0]+'" id="hapusPenerimaKepada">X</button></div></div>';
		        }
		        $$("#bagianPenerimaCompose").html(append);
		});

		$$(document).on('click','#tambahBagianPenerimaTembusanCompose',function(e){
			e.preventDefault();
			e.stopImmediatePropagation();
			var valueBagian = $$("#tembusanCompose option:checked").attr('data-name');
			var value = $$("#tembusanCompose").val();
			if(value != "")
			{
			  const { length } = arrTembusan;
			  const id = length + 1;
			  const found = arrTembusan.some(el => el[0] === value);
			  	if (!found)
			  	{

					arrTembusan.push([value,valueBagian]);
					console.log(arrTembusan);
			  		var append = '<div class="row"><div class="col-66"><small> - '+valueBagian+'</small></div><div class="col-33 container"> <button class="col button button-small button-fill bg-red" data-id="'+value+'" id="hapusPenerimaCompose">X</button></div></div>';
					$$("#bagianPenerimaTembusanCompose").append(append);
			  	}
			}
			
		})

		$$(document).on('click','#hapusPenerimaCompose',function(e){
			e.preventDefault();
			e.stopImmediatePropagation();
			var idPenerima = $$(this).data('id');
		        var idDihapus = idPenerima
		        arrTembusan = $.grep(arrTembusan, function(value)
		        	{
		        		if(value[0]!= idDihapus)
		        		{
		        			return value;
		        		}
					});
		        var append = "";
		        for (var i = 0; i < arrTembusan.length; i++) 
		        {
		        	append = append + '<div class="row"><div class="col-66"><small> - '+arrTembusan[i][1]+'</small></div><div class="col-33 containerr"> <button class="col button button-small button-fill bg-red" data-id="'+arrTembusan[i][0]+'" id="hapusPenerimaKepada">X</button></div></div>';
		        }
		        $$("#bagianPenerimaTembusanCompose").html(append);
		});
	
	}
});


//function2 tmbahan
async function homePageGetDashboardData(){
	var role = localStorage.role;
	if(localStorage.username == "admin")
	{
		role = "superadmin";
	}
    var kolokDipilih = $$("#selectKolok").val();
    localStorage.kolok = kolokDipilih;
    app.preloader.show();
    var urlRequest = url+'/api/inbox/tabel_inbox/'+localStorage.user_id;
    app.request({
        url: urlRequest,
        method:'post',
        async: false,
        headers:
            {
                'Authorization': 'Bearer ' + localStorage.apiToken
            }
        ,
        contentType:'application/json',
        data:{
            'role':role,
            'kolok':kolokDipilih
        },
        success:function(data){
			
            var returned = JSON.parse(data);
            totalKotakMasuk = returned['data'].length;

            $$("#jumlahInbox").html(totalKotakMasuk);
            localStorage.inboxTotal = totalKotakMasuk;
        },
        error: function(err) {
            console.log(err);
        }
    })

    var urlRequest = url+'/api/dashboard/div_disposisi/'+localStorage.user_id;
    app.request({
        url: urlRequest,
        method:'post',
        async: false,
        headers:
            {
                'Authorization': 'Bearer ' + localStorage.apiToken
            }
        ,
        contentType:'multipart/form-data',
        data:{
            'role':role,
            'kolok':kolokDipilih
        },
        success:function(data){
            var returned = JSON.parse(data);
            totalDisposisi =  returned['data'].length;

            $$("#jumlahDisposisi").html(totalDisposisi);
            localStorage.disposisiTotal = totalDisposisi;
        },
        error: function(err) {
            console.log(err);
        }
    })

    var urlRequest = url+'/api/kotak_keluar/div_kotak_keluar/'+localStorage.user_id;
    app.request({
        url: urlRequest,
        method:'post',
        async: false,
        headers:
            {
                'Authorization': 'Bearer ' + localStorage.apiToken
            }
        ,
        contentType:'multipart/form-data',
        data:{
            'role':role,
            'kolok':kolokDipilih
        },
        success:function(data){
            var returned = JSON.parse(data);
            totalKotakKeluar =  returned['data'].length;
			
            $$("#jumlahOutbox").html(totalKotakKeluar);
            localStorage.outboxTotal = totalKotakKeluar;
        },
        error: function(err) {
            console.log(err);
        }
    })
    setTimeout(function(){ app.preloader.hide(); }, 1500);
}

function checkAllowDisposisi(){
	// if (session('role_id') == 'kabid' || session('role_id') == 'kadis' || session('role_id') == 'kasie' || session('role_id') == 'superadmin' || session('role_id') == 'sekdis' || session('role_id') == 'kasubag' || session('role_id') == 'lurah') {
	if(localStorage.tipe != "outbox" && (localStorage.role == 'kabid' || localStorage.role == 'kadis' || localStorage.role == 'kasie' || localStorage.role == 'superadmin' || localStorage.role == 'sekdis' || localStorage.role == 'kasubag' || localStorage.role == 'lurah'  ) ){
		localStorage.tglsurat = $$(".tanggalsurat").val();
		mainView.router.navigate("/ds_popupdisposisi/");
	}
	else{
		app.dialog.alert("Maaf anda tidak dapat mendisposisi surat ini")
	}
}

async function tambahSuratKeluar(){
	var urlRequest = url+"/api/kotak_masuk/simpan_tulis_surat_baru";
	var form_data = new FormData();
	form_data.append("_token", 'PtcnynSsQ6nJvcagHKE3RkXTrgRsrlbaGSEdq6Xv');
	form_data.append("tipeSurat", '2');
	form_data.append("id_uploader", localStorage.user_id);
	form_data.append("unit_kerja_id", localStorage.kolok);
	
	
	// Pengecekan
	// if(!$("#asalNaskahCompose").val()){
	// 	app.dialog.alert('Asal Naskah Wajib Diisi');
	// 	return;
	// }
	// else if(!$("#tanggalSuratCompose").val()){
	// 	app.dialog.alert('Tanggal Surat Wajib Diisi');
	// 	return;
	// }
	// else if(!$("#noSuratCompose").val()){
	// 	app.dialog.alert('Nomor Surat Wajib Diisi');
	// 	return;
	// } 
	// else if(!$("#perihalSuratCompose").val()){
	// 	app.dialog.alert('Perihal Wajib Diisi');
	// 	return;
	// }
	// else if(arrKepada.length <= 0){
	// 	app.dialog.alert('Disampaikan Kepada Wajib Diisi');
	// 	return;
	// }
	// else if(!$("#tulisSuratKeluar").val()){
	// 	app.dialog.alert('Tulis Surat Wajib Diisi');
	// 	return;
	// }
	//end pengecekan awal
	
	if($("#nomorAgenda").val()){
		form_data.append("nomor_agenda", $("#nomorAgenda").val());
	}
	if($("#jenisSuratCompose").val()){
		form_data.append("jenisSuratMasuk", $("#jenisSuratCompose").val());
	}
	if(arrTembusan.length >0){
		let insToArrayTembusan = "["
		for (var i = 0; i < arrTembusan.length; i++) 
		{
			
			// insertToArray.push( parseInt(arr[i][0]) )
			if(i == arrTembusan.length-1)
			{
				insToArrayTembusan = insToArrayTembusan + arrTembusan[i][0];
			}
			else
			{
				insToArrayTembusan = insToArrayTembusan + arrTembusan[i][0]+",";
			}
		}
		insToArrayTembusan = insToArrayTembusan + "]";
		form_data.append("tembusan", insToArrayTembusan);
	}
	if($("#asalNaskahCompose").val()){
		form_data.append("asal_naskah", $("#asalNaskahCompose").val());
	}
	if($("#tanggalSuratCompose").val()){
		let tglraw =  ($("#tanggalSuratCompose").val()).split("-");
		let tglmasukvaldb = tglraw[2] + "-" + tglraw[1] + "-" + tglraw[0];
		form_data.append("tanggal_surat",$("#tanggalSuratCompose").val());
		form_data.append("tanggal_masuk_surat",$("#tanggalSuratCompose").val());
	}  
	if($("#noSuratCompose").val()){
		form_data.append("nomor_surat", $("#noSuratCompose").val());
	}
	if($("#lampiranKeluar").val()){
		form_data.append("total_file", 1);
		form_data.append("filename0", document.getElementById("lampiranKeluar").files[0]);
		// form_data.append("lampiran", $("#lampiranKeluar").val());
	}
	if($("#perihalSuratCompose").val()){
		form_data.append("perihal", $("#perihalSuratCompose").val());
	}
	if(arrKepada.length >0){
		let insToArrayKepada = "["
		for (var i = 0; i < arrKepada.length; i++) 
		{
			
			// insertToArray.push( parseInt(arr[i][0]) )
			if(i == arrKepada.length-1)
			{
				insToArrayKepada = insToArrayKepada + arrKepada[i][0];
			}
			else
			{
				insToArrayKepada = insToArrayKepada + arrKepada[i][0]+",";
			}
		}
		insToArrayKepada = insToArrayKepada + "]";
		form_data.append("id_kepada_array_1", insToArrayKepada);
	}
	if($("#tingkatPerkembangan").val()){
		form_data.append("tingkat_perkembangan_id", $("#tingkatPerkembangan").val());
	}
	if($("#tingkatUrgentsi").val()){
		form_data.append("tingkat_urgentsi_id", $("#tingkatUrgentsi").val());
	}
	if($("#sifat_surat_id").val()){
		form_data.append("tingkat_urgentsi_id", $("#sifatSurat").val());
	}
	
	if($("#namaAcara").val()){
		form_data.append("kegiatan", $("#namaAcara").val());
	}
	if($(".waktuMulai").val() && $$('#checkboxMasterData').is(":checked")){
		let waktuparser = dateParser($(".waktuMulai").val());
		let timespliter = waktuparser.split(" ");
		form_data.append("tglAcaraMulai", timespliter[0]);
		form_data.append("jamMulai", timespliter[1]);
	}
	if($(".waktuSelesai").val() && $$('#checkboxMasterData').is(":checked")){
		let waktuparser = dateParser($(".waktuSelesai").val());
		let timespliter = waktuparser.split(" ");
		form_data.append("tglAcaraSelesai", timespliter[0]);
		form_data.append("jamSelesai", timespliter[1]);
	}
	if($("#namaAcara").val()){
		form_data.append("kegiatan", $("#namaAcara").val());
	}
	if($("#tempatAcara").val()){
		form_data.append("tempat_kegiatan", $("#tempatAcara").val());
	}
	if($("#tempatAcara").val()){
		form_data.append("tempat_kegiatan", $("#tempatAcara").val());
	}
	if($("#tulisSuratKeluar").val()){
		form_data.append("tulis_surat", $("#tulisSuratKeluar").val());
	}


	$.ajax({
		url: urlRequest,
		method: "POST",
		headers:
			{
				'Authorization': 'Bearer ' + localStorage.apiToken
			}
		,
		data : form_data,
		async:false,
		contentType: false,
		cache: false,
		processData: false,
		
		beforeSend:function(){
			app.preloader.show();
		},
		success: function (data) {
			app.preloader.hide();
			app.dialog.alert('tambah surat keluar berhasil');
			setTimeout(function(){
				mainView.router.navigate("/home/");
			}, 500);
			
			
		},
		error: function(jqXHR, textStatus, errorThrown) {
			app.preloader.hide();
			app.dialog.alert('tambah surat keluar gagal');
			console.log(errorThrown);
		}
	});
}


async function tambahSuratMasuk(){
	var urlRequest = url+"/api/kotak_masuk/simpan_surat_baru";
	var form_data = new FormData();
	form_data.append("_token", 'PtcnynSsQ6nJvcagHKE3RkXTrgRsrlbaGSEdq6Xv');
	form_data.append("tipeSurat", '1');
	form_data.append("id_uploader", localStorage.user_id);
	form_data.append("unit_kerja_id", localStorage.kolok);
	
	
	// Pengecekan
	if(!$("#nomorAsalNaskah").val()){
		app.dialog.alert('Nomor Asal Naskah Wajib Diisi');
		return;
	}
	else if(!$("#tanggalMasukSuratCompose").val()){
		app.dialog.alert('Tanggal Masuk Surat Wajib Diisi');
		return;
	}
	else if(!$("#asalNaskahCompose").val()){
		app.dialog.alert('Asal Naskah Wajib Diisi');
		return;
	}
	else if(!$("#tanggalSuratCompose").val()){
		app.dialog.alert('Tanggal Surat Wajib Diisi');
		return;
	}
	else if(!$("#noSuratCompose").val()){
		app.dialog.alert('Nomor Surat Wajib Diisi');
		return;
	} 
	else if(!$("#perihalSuratCompose").val()){
		app.dialog.alert('Perihal Wajib Diisi');
		return;
	}
	else if(arrKepada.length <= 0){
		app.dialog.alert('Disampaikan Kepada Wajib Diisi');
		return;
	}
	//end pengecekan awal
	
	if($("#nomorAgenda").val()){
		form_data.append("nomor_agenda", $("#nomorAgenda").val());
	}
	if($("#nomorAsalNaskah").val()){
		form_data.append("nomor_resi", $("#nomorAsalNaskah").val());
	}
	if($("#tanggalMasukSuratCompose").val()){
		let tglraw =  ($("#tanggalMasukSuratCompose").val()).split("-");
		let tglmasukvaldb = tglraw[2] + "-" + tglraw[1] + "-" + tglraw[0];
		form_data.append("tanggal_masuk_surat", $("#tanggalMasukSuratCompose").val());
	}
	if($("#jenisSuratCompose").val()){
		form_data.append("jenisSuratMasuk", $("#jenisSuratCompose").val());
	}
	if(arrTembusan.length >0){
		let insToArrayTembusan = "["
		for (var i = 0; i < arrTembusan.length; i++) 
		{
			
			// insertToArray.push( parseInt(arr[i][0]) )
			if(i == arrTembusan.length-1)
			{
				insToArrayTembusan = insToArrayTembusan + arrTembusan[i][0];
			}
			else
			{
				insToArrayTembusan = insToArrayTembusan + arrTembusan[i][0]+",";
			}
		}
		insToArrayTembusan = insToArrayTembusan + "]";
		form_data.append("tembusan", insToArrayTembusan);
	}
	if($("#asalNaskahCompose").val()){
		form_data.append("asal_naskah", $("#asalNaskahCompose").val());
	}
	if($("#tanggalSuratCompose").val()){
		let tglraw =  ($("#tanggalSuratCompose").val()).split("-");
		let tglmasukvaldb = tglraw[2] + "-" + tglraw[1] + "-" + tglraw[0];
		form_data.append("tanggal_surat",$("#tanggalSuratCompose").val());
	}  
	if($("#noSuratCompose").val()){
		form_data.append("nomor_surat", $("#noSuratCompose").val());
	}
	if($("#lampiranCompose").val()){
		form_data.append("lampiran", $("#lampiranCompose").val());
	}
	if($("#perihalSuratCompose").val()){
		form_data.append("perihal", $("#perihalSuratCompose").val());
	}
	if(arrKepada.length >0){
		let insToArrayKepada = "["
		for (var i = 0; i < arrKepada.length; i++) 
		{
			
			// insertToArray.push( parseInt(arr[i][0]) )
			if(i == arrKepada.length-1)
			{
				insToArrayKepada = insToArrayKepada + arrKepada[i][0];
			}
			else
			{
				insToArrayKepada = insToArrayKepada + arrKepada[i][0]+",";
			}
		}
		insToArrayKepada = insToArrayKepada + "]";
		form_data.append("id_kepada_array_1", insToArrayKepada);
	}
	if($("#tingkatPerkembangan").val()){
		form_data.append("tingkat_perkembangan_id", $("#tingkatPerkembangan").val());
	}
	if($("#tingkatUrgentsi").val()){
		form_data.append("tingkat_urgentsi_id", $("#tingkatUrgentsi").val());
	}
	if($("#sifat_surat_id").val()){
		form_data.append("tingkat_urgentsi_id", $("#sifatSurat").val());
	}
	if($("#file1").val()){
		form_data.append("filename", document.getElementById("file1").files[0]);
	}
	if($("#file2").val()){
		form_data.append("filename2", document.getElementById("file2").files[0]);
	}
	if($("#file3").val()){
		form_data.append("filename3", document.getElementById("file3").files[0]);
	}
	if($("#file4").val()){
		form_data.append("filename4", document.getElementById("file4").files[0]);
	}
	if($("#file5").val()){
		form_data.append("filename5", document.getElementById("file5").files[0]);
	}
	
	if($("#namaAcara").val()){
		form_data.append("kegiatan", $("#namaAcara").val());
	}
	if($(".waktuMulai").val()){
		let waktuparser = dateParser($(".waktuMulai").val());
		let timespliter = waktuparser.split(" ");
		form_data.append("tglAcaraMulai", timespliter[0]);
		form_data.append("jamMulai", timespliter[1]);
	}
	if($(".waktuSelesai").val()){
		let waktuparser = dateParser($(".waktuSelesai").val());
		let timespliter = waktuparser.split(" ");
		form_data.append("tglAcaraSelesai", timespliter[0]);
		form_data.append("jamSelesai", timespliter[1]);
	}
	if($("#namaAcara").val()){
		form_data.append("kegiatan", $("#namaAcara").val());
	}
	if($("#tempatAcara").val()){
		form_data.append("tempat_kegiatan", $("#tempatAcara").val());
	}


	$.ajax({
		url: urlRequest,
		method: "POST",
		headers:
			{
				'Authorization': 'Bearer ' + localStorage.apiToken
			}
		,
		data : form_data,
		async:false,
		contentType: false,
		cache: false,
		processData: false,
		
		beforeSend:function(){
			app.preloader.show();
		},
		success: function (data) {
			app.preloader.hide();
			app.dialog.alert('tambah surat masuk berhasil');
			setTimeout(function(){
				mainView.router.navigate("/home/");
			}, 500);
			
			
		},
		error: function(jqXHR, textStatus, errorThrown) {
			app.preloader.hide();
			app.dialog.alert('tambah surat masuk gagal');
			console.log(errorThrown);
		}
	});
}

function parseJwt (token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
}

// function checkPermissionCallback(status) {
// 	if (!status.hasPermission) {
// 		var errorCallback = function () {
// 			console.warn('Storage permission is not turned on');
// 		}
// 		permissions.requestPermission(
// 		  permissions.READ_EXTERNAL_STORAGE,
// 		  function (status) {
// 			  if (!status.hasPermission) {
// 				  errorCallback();
// 			  } else {
// 				  // continue with downloading/ Accessing operation 
// 				  $scope.downloadFile();
// 			  }
// 		  },
// 		  errorCallback);
// 	}
// }

async function checkLogin() {
	var permissions = cordova.plugins.permissions;
	permissions.requestPermission(permissions.READ_EXTERNAL_STORAGE, success, error);

	function error() {
	console.log('Camera permission is not turned on');
	}

	function success( status ) {
	if( !status.hasPermission ) error();
	}
	// var permissions = cordova.plugins.permissions;
	// permissions.hasPermission(permissions.READ_EXTERNAL_STORAGE, checkPermissionCallback, null);
	//set url api tte
	var urlapi = url+"/api/get_url_tte";
	app.request({
		url: urlapi,
		method:'GET',
		contentType:'multipart/form-data',
		success:function(data){
		var returned = JSON.parse(data);
		var data = returned['data'];
		API_TTE = data['value'] +"/";
		API_TTE_PASSWORD = data['pass'];
		API_TTE_USERNAME = data['username'];
		
		},
		error: function(err) {
		app.dialog.alert('Failed to get url api tte');
		}
	})
	if(!localStorage.apiToken){
		console.log('masuk if 1');
		app.preloader.show();
		setTimeout(function(){
			app.preloader.hide();
			localStorage.clear();
		}, 1000);
	}
	else{
		app.preloader.show();
		console.log('masuk if 2');
		let tokenJWT = parseJwt(localStorage.apiToken);
		var d = new Date();
		var n = d.getTime();
		var nFix = parseInt(String(n).substring(0,10));
		
		if(tokenJWT['exp'] <= nFix){
			app.preloader.show();
			setTimeout(function(){
				app.preloader.hide();
				localStorage.clear();
			}, 1000);
		}else{
			setTimeout(function(){
				mainView.router.navigate("/home/");
			}, 1500);
		}
	}
};

async function loadHomeData(){
	console.log("func loadhome data called")
	
}

function dateParser(param=''){
	let paramsplit = param.split(" ");
	let numBulan = "";
	//            
	
	if(paramsplit[0] == "January"){
		numBulan = "01"
	}
	else if(paramsplit[0] == "February"){
		numBulan = "02"
	}
	else if(paramsplit[0] == "March"){
		numBulan = "03"
	}
	else if(paramsplit[0] == "April"){
		numBulan = "04"
	}
	else if(paramsplit[0] == "May"){
		numBulan = "05"
	}
	else if(paramsplit[0] == "June"){
		numBulan = "06"
	}
	else if(paramsplit[0] == "July"){
		numBulan = "07"
	}
	else if(paramsplit[0] == "August"){
		numBulan = "08"
	}
	else if(paramsplit[0] == "September"){
		numBulan = "09"
	}
	else if(paramsplit[0] == "October"){
		numBulan = "10"
	}
	else if(paramsplit[0] == "November"){
		numBulan = "11"
	}
	else if(paramsplit[0] == "December"){
		numBulan = "12"
	}
	let numTanggal = paramsplit[1].replaceAll(",", "");
	if (numTanggal.length == 1){
		numTanggal = "0" + numTanggal;
	}

	return paramsplit[2] + "-" + numBulan + "-" +  numTanggal + " " + paramsplit[3] + ":00";
}

async function isiDataComposeSurat(){
	var urlRequest = url+"/api/kotak_masuk/get_jenis_surat_masuk";
	app.request({
		url: urlRequest,
		method:'post',
		headers:
		{
			'Authorization': 'Bearer ' + localStorage.apiToken
		}
		,
		contentType:'multipart/form-data',
		data:{
		
		},
		success:function(data){
		var returned = JSON.parse(data);
		var data = returned['data'];
		// console.log(data); //console jenis naskah
		var append = '<option value="">-</option>';
		for (var i = 0; i <= data.length; i++)
		{
			if(data[i] != null)
			{
				// console.log(data[i]);
				append = append + '<option value="'+data[i]['id']+'">'+data[i]['type_name']+'</option>';
			}
		}
		$$("#jenisSuratCompose").html(append);
		},
		error: function(err) {
		app.dialog.alert('Failed to  get "JENIS_SURAT"');
		}
	})

	var urlRequestPerkembangan = url+"/api/tingkat_perkembangan/get_tingkat_perkembangan";
	app.request({
		url: urlRequestPerkembangan,
		method:'post',
		headers:
		{
			'Authorization': 'Bearer ' + localStorage.apiToken
		}
		,
		contentType:'multipart/form-data',
		data:{
		
		},
		success:function(data){
		var returned = JSON.parse(data);
		var data = returned['data'];
		// console.log(data); //console tingkat perkembangan
		var append = '<option value="">-</option>';
		for (var i = 0; i <= data.length; i++)
		{
			if(data[i] != null)
			{
				// console.log(data[i]);
				append = append + '<option value="'+data[i]['id']+'">'+data[i]['tingkat_perkembangan']+'</option>';
			}
		}
		$$("#tingkatPerkembangan").html(append);
		},
		error: function(err) {
		app.dialog.alert('Failed to  get "tingkat perkembangan"');
		}
	})

	var urlRequestUrgentsi = url+"/api/tingkat_urgentsi/get_tingkat_urgentsi";
	app.request({
		url: urlRequestUrgentsi,
		method:'post',
		headers:
		{
			'Authorization': 'Bearer ' + localStorage.apiToken
		}
		,
		contentType:'multipart/form-data',
		data:{
		
		},
		success:function(data){
		var returned = JSON.parse(data);
		var data = returned['data'];
		// console.log(data); //console tingkat urgentsi
		var append = '<option value="">-</option>';
		for (var i = 0; i <= data.length; i++)
		{
			if(data[i] != null)
			{
				// console.log(data[i]);
				append = append + '<option value="'+data[i]['id']+'">'+data[i]['tingkat_urgentsi']+'</option>';
			}
		}
		$$("#tingkatUrgentsi").html(append);
		},
		error: function(err) {
		app.dialog.alert('Failed to  get "tingkat perkembangan"');
		}
	})

	var urlRequestSifat = url+"/api/master/sifat_surat/show_all/";
	app.request({
		url: urlRequestSifat,
		method:'GET',
		headers:
		{
			'Authorization': 'Bearer ' + localStorage.apiToken
		}
		,
		contentType:'multipart/form-data',
		data:{
		
		},
		success:function(data){
		var returned = JSON.parse(data);
		var data = returned['data'];
		// console.log(data); //console sifat surat 
		var append = '<option value="">-</option>';
		for (var i = 0; i <= data.length; i++)
		{
			if(data[i] != null)
			{
				// console.log(data[i]);
				append = append + '<option value="'+data[i]['id']+'">'+data[i]['nama_sifat']+'</option>';
			}
		}
		$$("#sifatSurat").html(append);
		},
		error: function(err) {
		app.dialog.alert('Failed to  get "sifat surat"');
		}
	})
}

function downloadFile(fileUri, namafile) {
	var fileTransfer = new FileTransfer();
	var uri = encodeURI(fileUri);
	var trustHosts = true

	var targetPath = cordova.file.externalDataDirectory + 'Download/'  + namafile;
 
	fileTransfer.download(
	   uri, 
	   targetPath, 
	   function(entry) {
		  console.log("download complete: " + entry.toURL());
		  app.dialog.alert('cdownload sukses' + entry.toURL());
	   },
		 
	   function(error) {
		  console.log("download error source " + error.source);
		  console.log("download error target " + error.target);
		  console.log("download error code" + error.code);
		  console.log(error);
		  console.log(JSON.stringify(error));
		  app.dialog.alert("c" + JSON.stringify(error));
	   },
	   trustHosts
	);
 }

function moveFile(fileUri) {
    var link = document.createElement("a");
    // If you don't know the name or want to use
    // the webserver default set name = ''
    link.setAttribute('download', name);
    link.href = uri;
    document.body.appendChild(link);
    link.click();
    link.remove();
	
}
function success(){
	console.log('download success')
}

var errorCallback = function(e) {
    app.dialog.alert('Failed to download file :' + e);
    console.log("Error: " + e)
    
}