function getShortDateMonthName(param=''){
    let result = '';
    var splittime = param.split("-"); //2020-12-24
    var bulan ='';
    if(splittime[1] == "01")
        bulan = "JAN";
    else if(splittime[1] == "02")
        bulan = "FEB";
    else if(splittime[1] == "03")
        bulan = "MAR";
    else if(splittime[1] == "04")
        bulan = "APR";
    else if(splittime[1] == "05")
        bulan = "MEI";
    else if(splittime[1] == "06")
        bulan = "JUN";
    else if(splittime[1] == "07")
        bulan = "JUL";
    else if(splittime[1] == "08")
        bulan = "AUG";
    else if(splittime[1] == "09")
        bulan = "SEPT";
    else if(splittime[1] == "10")
        bulan = "OCT";
    else if(splittime[1] == "11")
        bulan = "NOV";
    else if(splittime[1] == "12")
        bulan = "DES";

    result = splittime[2] + ' ' + bulan;   
    return result;
}

function uploadFile() {

    var files = document.getElementById("ds_file").files;
 
    if(files.length > 0 ){
 
       var formData = new FormData();
       formData.append("file", files[0]);
 
       var xhttp = new XMLHttpRequest();
 
       // Set POST method and ajax file path
       xhttp.open("POST", "https://dev2-esurat.madiunkota.go.id/api/upload_file.php", true);
 
       // call on request changes state
       xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
 
            var response = this.responseText;
            if(response == 1){
               alert("Upload successfully.");
            }else{
               alert("File not uploaded.");
            }
          }
       };
 
       // Send request with data
       xhttp.send(formData);
 
    }else{
       alert("Please select a file");
    }
 
 }